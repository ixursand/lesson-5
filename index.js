// birinchi
function countVowel(text){
    let count = 0;
    let arrText = text.split();
    for(let i=0; i<=text.length; i++){
        if(text[i]=='a' || text[i]=='i' || text[i]=='u' || text[i]=='o' || text[i]=='e' || text[i]=='u'){
            count += 1;
        }
    }
    return count;
}

console.log(countVowel('The quick brown fox'));

// ikkinchi
var one_day = 1000 * 60 * 60 * 24
var present_date = new Date();
var yangi_yil = new Date(present_date.getFullYear(), 12, 31)
var Result = Math.round(yangi_yil.getTime() - present_date.getTime()) / (one_day);
var Final_Result = Result.toFixed(0);

console.log("Bugun "
            + present_date + "dan yangi yil"
            + yangi_yil + " gacha "
            + Final_Result+
            " kun bor") ;


// uchinchi
function findSymbol(value, arr){
    let status = 'Topilmadi';

    for (let i=0; i<=arr.length; i++){
        let name = arr[i];
        if(name==value){
            status = 'Topildi';
            break;
        }
    }
    return status;
}

let sym_arr = ['@', '#', '%'];
console.log('Natija: '+findSymbol('@',sym_arr));

// to'rtinchi
function commonValues(arr1,arr2){
    let values = [];
    for(let i=0; i<=arr1.length; i++){
        for(let j=0; j<=arr2.length; j++){
            if(arr1[i] === arr2[j]){
                values.push(arr1[i])
            }
        }
    }
    return values;
}

var arr11 = ["Saidakbar", "Saidamir", "Oybek", "Farruh", "Xursand"];
var arr22 = ["Kamila","Elyor", "Saidakbar", "Jamshid", "Xursand", "Khusan", "Azizullo"];
console.log(commonValues(arr11,arr22))

// beshinchi
window.onload = setTimeout(function(){
    alert('This is an alert');
    window.location = 'http://www.kun.uz';
}, 4000);

// oltinchi
function nSum(arr){
    let sum = 0;
    for(let i=0; i<=arr.length; i++){
        if((arr[i] >= 0.0) && (Math.floor(arr[i]) === arr[i]) && arr[i] !== Infinity){
            sum += arr[i]
        }
    }
    return sum
}

let text = [23 , 'dfv' , 23.4 ,null , 6, 2]
console.log(nSum(text))


// yettinchi
function givenChar(stringText){
    let count = stringText.split('i').length-1;
    let result = 'i ' + count + ' marta qatnashgan';

    return result;
}

console.log(givenChar('HeLLo UdEvS.io'))