# Lesson 5

1. Write a JavaScript function that accepts a string as a parameter and counts the
number of vowels within the string.
Note : As the letter 'y' can be regarded as both a vowel and a consonant, we do
not count 'y' as vowel here.<br>
Sample Data and output:<br>
Example string : 'The quick brown fox'<br>
Expected Output : 5<br>

2. Write a JavaScript program to calculate number of days left until next New year. <br><br>


3. Write a JavaScript program to find Symbol values ( @, #, %....) in a JavaScript array. <br><br>


4. Write a JavaScript program to compute the intersection of two arrays. <br><br>


5. Write a JavaScript program to alert message after 4 seconds ( use browser alert method) <br><br>

6. Write a Js program to sum natural number in an array. <br><br>

7. Write a Js program to calculate specific char in a given string. <br><br>
    Input: function ('there was message which had sent b```y``` m```y``` side,  y) <br>
    Output: 2 <br>


